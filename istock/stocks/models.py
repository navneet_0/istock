from django.db import models
from django.db.models import F


from api.v0.utils.unity_root import get_unity_number_for_scrip, cal_unity_root

# Create your models here.


class Stock(models.Model):
    stock_code = models.CharField(max_length=100, unique=True, primary_key=True)
    stock_name = models.CharField(max_length=200, null=True, blank=True)
    unity_root = models.IntegerField(
        null=True,
        blank=True,
        db_index=True,
        db_column='unity_root',
        db_comment='Unity root for the stock code',
    )

    def save(self, *args, **kwargs):
        self.unity_root = get_unity_number_for_scrip(self.stock_code)
        super(Stock, self).save(*args, **kwargs)

    def __str__(self):
        return self.stock_code

    @property
    def name_unity_root(self):
        return get_unity_number_for_scrip(self.stock_code)


class StockDetails(models.Model):
    # NSE = 'NSE'
    # BSE = 'BSE'
    # EXCHANGE_OPTIONS = (
    #     'NSE'  NSE,
    #     'BSE' : BSE
    # )
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    stock_exchange = models.CharField(max_length=100, null=True, blank=True)
    unity_root = models.IntegerField(default=0)
    date_of_birth =models.DateField(null=True, blank=True)
    life_high = models.IntegerField(default=0)
    life_low = models.IntegerField(default=0)
    high_year = models.IntegerField(default=0)
    low_year = models.IntegerField(default=0)


class DailyTransactions(models.Model):
    trade_date = models.DateField(null=True, blank=True)
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    previous_close = models.FloatField(default=0)
    day_open = models.FloatField(default=0)
    day_high = models.FloatField(default=0)
    day_low = models.FloatField(default=0)
    day_close = models.FloatField(default=0)
    vwap = models.FloatField(default=0)
    volume_traded = models.FloatField(default=0)
    volume_delivered = models.FloatField(default=0)
    traded_value = models.FloatField(default=0)
    ratio_delv_to_traded = models.FloatField(default=0)

    class Meta:
        unique_together = ('trade_date', 'stock')
        ordering = ['trade_date']

    @property
    def volume_traded_ratio(self):
        # Get the previous day's transaction for the same stock
        previous_day = DailyTransactions.objects.filter(
            stock=self.stock,
            trade_date__lt=self.trade_date
        ).order_by('-trade_date').first()

        if previous_day and previous_day.volume_traded > 0:
            ratio = self.volume_traded / previous_day.volume_traded
            return round(ratio, 2)
        return None  # or 0, if you prefer to return 0 when there's no previous transaction

    @property
    def volume_delivered_ratio(self):
        # Get the previous day's transaction for the same stock
        previous_day = DailyTransactions.objects.filter(
            stock=self.stock,
            trade_date__lt=self.trade_date
        ).order_by('-trade_date').first()

        if previous_day and previous_day.volume_delivered > 0:
            ratio = self.volume_delivered / previous_day.volume_delivered
            return round(ratio, 2)
        return None

    @property
    def gap_opening(self):
        if self.previous_close < self.day_low:
            if self.day_open > self.previous_close:
                return round(self.day_open - self.previous_close, 2)
            else:
                return 0
        else:
            return 0

    @property
    def opening_price_unity_root(self):
        return cal_unity_root(self.day_open, 0)

    @property
    def closing_price_unity_root(self):
        return cal_unity_root(self.day_close, 0)

    @property
    def high_price_unity_root(self):
        return cal_unity_root(self.day_high, 0)

    @property
    def low_price_unity_root(self):
        return cal_unity_root(self.day_low, 0)

    @property
    def current_week(self):
        today = self.trade_date
        week_num = today.isocalendar()[1]
        return week_num


class WeeklyData(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    week_number = models.IntegerField(default=0)
    year = models.IntegerField(default=0)
    week_start = models.DateField(null=True, blank=True)
    week_end = models.DateField(null=True, blank=True)
    week_open = models.FloatField(default=0)
    week_high = models.FloatField(default=0)
    week_low = models.FloatField(default=0)
    week_close = models.FloatField(default=0)
    week_vwap = models.FloatField(default=0)
    week_volume = models.FloatField(default=0)
    week_delivered = models.FloatField(default=0)
    week_traded_value = models.FloatField(default=0)
    week_delv_to_traded = models.FloatField(default=0)

    class Meta:
        unique_together = ('stock', 'week_number', 'year')
        ordering = ['year', 'week_number']

    @property
    def week_delv_to_traded(self):
        if self.week_volume > 0:
            ratio = self.week_delivered / self.week_volume
            return round(ratio, 2)
        return None

    @property
    def week_price_range_ratios(self):
        if self.week_high > 0 and self.week_low > 0:
            week_range = self.week_high - self.week_low
            upper_cut = self.week_high - self.week_open
            lower_cut = self.week_open - self.week_low
            real_body = self.week_close - self.week_open
            upper_wick_ratio = upper_cut / week_range
            lower_wick_ratio = lower_cut / week_range
            real_body_ratio = real_body / week_range
            return {
                'upper_wick_ratio': round(upper_wick_ratio, 2),
                'lower_wick_ratio': round(lower_wick_ratio, 2),
                'real_body_ratio': round(real_body_ratio, 2)
            }

        return None


class StockDeals(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    deal_date = models.DateField(null=True, blank=True)
    deal_type = models.CharField(max_length=100, null=True, blank=True)
    deal_price = models.FloatField(default=0)
    deal_volume = models.FloatField(default=0)
    deal_value = models.FloatField(default=0)
    target_price = models.FloatField(default=0)
    sold_price = models.FloatField(default=0)
    net_profit = models.FloatField(default=0)
    deal_status = models.CharField(max_length=100, null=True, blank=True)
    target_date = models.DateField(null=True, blank=True)


