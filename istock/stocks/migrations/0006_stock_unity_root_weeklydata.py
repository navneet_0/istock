# Generated by Django 5.0 on 2024-07-06 11:14

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("stocks", "0005_dailytransactions_previous_close_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="stock",
            name="unity_root",
            field=models.IntegerField(
                blank=True,
                db_column="unity_root",
                db_comment="Unity root for the stock code",
                db_index=True,
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="WeeklyData",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("week_number", models.IntegerField(default=0)),
                ("year", models.IntegerField(default=0)),
                ("week_start", models.DateField(blank=True, null=True)),
                ("week_end", models.DateField(blank=True, null=True)),
                ("week_high", models.FloatField(default=0)),
                ("week_low", models.FloatField(default=0)),
                ("week_close", models.FloatField(default=0)),
                ("week_vwap", models.FloatField(default=0)),
                ("week_volume", models.FloatField(default=0)),
                ("week_delivered", models.FloatField(default=0)),
                ("week_traded_value", models.FloatField(default=0)),
                ("week_delv_to_traded", models.FloatField(default=0)),
                (
                    "stock",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="stocks.stock"
                    ),
                ),
            ],
        ),
    ]
