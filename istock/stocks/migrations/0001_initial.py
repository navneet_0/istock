# Generated by Django 4.1 on 2023-01-10 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('stock_code', models.CharField(max_length=100, primary_key=True, serialize=False, unique=True)),
                ('stock_name', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
    ]
