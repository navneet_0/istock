import logo from "./logo.svg";
import "./App.css";

import { createBrowserRouter, RouterProvider } from "react-router-dom";
import HomePage from "./containers/HomePage";
import StockDetails from "./containers/StockDetails";
import StockList from "./containers/StockList";
import VolumeAnalysis from "./containers/VolumeAnalysis";
import DailyTransactions from "./containers/DailyTransactions";
import CurrentDeals from "./containers/CurrentDeals";
import WeeklyData from "./containers/WeeklyData";
import Dummy from "./containers/Dummy";

import LayoutHeader from "../src/components/LayoutHeader";
import CustomLayout from "../src/components/CustomLayout";

function App() {
  const router = createBrowserRouter([
    {
      // parent route component
      element: <CustomLayout
      className="layout-content"
  style={{
    minHeight: '280px',
    background: 'white',
  }}
      />,
      // child route components
      children: [
        {
          path: "/",
          element: <HomePage />,
        },
        // other pages....
        {
          path: "/details",
          element: <StockDetails />,
        },
        {
          path: "/list",
          element: <StockList />,
        },
        {
          path: "daily-transactions",
          element: <DailyTransactions />,
        },
        {
          path: "/volume",
          element: <VolumeAnalysis />
        },
        {
          path: "/current-deals",
          element: <CurrentDeals />
        },
        {
          path: "/weekly-data",
          element: <WeeklyData />
        },
        {
          path: "/dummy",
          element: <Dummy />
        },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
