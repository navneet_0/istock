
import React from "react";
import { Badge } from "antd";


// Function to format numbers with commas
const formatNumberWithCommas = (number) => {
  if (number > 10000000) {
    return `${(number / 10000000).toFixed(2)} Cr`;
  }
  else if (number > 100000) {
    return `${(number / 100000).toFixed(2)} Lac`;
  }
  else if (number > 1000) {
    return `${(number / 1000).toFixed(2)} K`;
  }
  // return new Intl.NumberFormat("en-US").format(number);
};


const displayVolTradeRatio = (value) => {
  if (value > 1.618) {
    return (
      // <span ><Text code
      // style={{ color: "blue", fontWeight: "bolder" }}
      // >{value}</Text></span>
      <Badge
        className="site-badge-count-109"
        count={value}
        style={{
          backgroundColor: '#52c41a',
        }}
      />
    );
  }
  return <span style={{ color: "black" }}>{value}</span>;
};

const displayDevRatio = (value) => {
  if (value > 61.8) {
    return (
      <span style={{ color: "green", fontWeight: "bolder" }}>
        <Badge
          className="site-badge-count-109"
          count={value}
          style={{
            backgroundColor: '#52c41a',
          }}
        />
      </span>
    );
  }
  return <span style={{ color: "black" }}>{value}</span>;
};


export { formatNumberWithCommas, displayVolTradeRatio, displayDevRatio };


