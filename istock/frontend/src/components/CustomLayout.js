import { Layout } from 'antd'; // Assuming you're using Ant Design Layout
import { Outlet } from 'react-router-dom';
import LayoutHeader from './LayoutHeader'; // Adjust the import according to your file structure

const { Content } = Layout;

export default function CustomLayout() {
  return (
    <Layout>
      <Layout.Header>
        <LayoutHeader />
      </Layout.Header>
      <Layout>
        <Content
          style={{
            padding: '24px',
            minHeight: '280px',
            background: 'white',
            margin: '0 auto',
            maxWidth: '1200px',
            boxSizing: 'border-box',
          }}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
}



// import { Outlet } from "react-router-dom";
// import { Breadcrumb, Layout, Menu, theme } from "antd";
//
// import LayoutHeader from "./LayoutHeader";
//
// export default function CustomLayout() {
//   return (
//     <>
//       <main>
//         <LayoutHeader />
//         <div
//           style={{
//             padding: 24,
//             minHeight: 280,
//             background: "white",
//             marginLeft: "200px",
//             marginRight: "200px",
//           }}
//         >
//           <Outlet />
//         </div>
//       </main>
//     </>
//   );
// }
