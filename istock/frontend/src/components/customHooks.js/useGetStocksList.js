import { useState, useEffect } from "react";

const useGetStocksList = () => {
  const [stocksList, setStocksList] = useState([]);

  useEffect(() => {
    const storedStocksList = localStorage.getItem("stocksList");
    if (storedStocksList) {
      try {
        setStocksList(JSON.parse(storedStocksList));
      } catch (error) {
        console.error("Error parsing stored stock list", error);
      }
    }
  }, []);

  return stocksList;
};

export default useGetStocksList;