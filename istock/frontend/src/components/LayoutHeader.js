import React from "react";
import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, theme } from "antd";

import { Link } from "react-router-dom";
import StockDetails from "../containers/StockDetails";
import HomePage from "../containers/HomePage";

const { Header, Content, Sider } = Layout;

const headerItems = [
  {
    key: 1,
    label: <Link to="/">Home</Link>,
  },
  {
    key: "2",
    label: <Link to="/details">Details</Link>,
  },
  {
    key: 3,
    label: <Link to="/list">List</Link>,
  },
  {
    key: 4,
    label: <Link to="/daily-transactions">Daily</Link>,
  },
  {
    key: 5,
    label: <Link to="/volume">Volume</Link>
  },
  {
    key: 6,
    label: <Link to="/current-deals">Current Deals</Link>
  },
  {
    key: 7,
    label: <Link to="/fundas">Trade Fundas</Link>
  },
];

const LayoutHeader = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Layout>
      <Header
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <div className="demo-logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["2"]}
          items={headerItems}
          style={{
            flex: 1,
            minWidth: 0,
          }}
        />
      </Header>
      {/* <Layout>
        <Sider
          width={200}
          style={{
            background: colorBgContainer,
          }}
        >
          <Menu
            mode="inline"
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            style={{
              height: '100%',
              borderRight: 0,
            }}
            items={items2}
          />
        </Sider>
        <Layout
          style={{
            padding: '0 24px 24px',
          }}
        >
          <Breadcrumb
            style={{
              margin: '16px 0',
            }}
          >
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>
            <Link
            to="/details"
            >
            Details
            </Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <Content
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            Content
          </Content>
        </Layout>
      </Layout> */}
    </Layout>
  );
};
export default LayoutHeader;
