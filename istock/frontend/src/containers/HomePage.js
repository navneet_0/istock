import React, { useState, state } from "react";

import {
  PlusOutlined,
  ArrowDownOutlined,
  ArrowUpOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  Row,
  Select,
  Space,
  FloatButton,
  notification,
  Card,
  Statistic,
} from "antd";

import axiosRequest from "../utils/axiosRequest";

const { Option } = Select;

const HomePage = () => {
  const [open, setOpen] = useState(false);
  const [payload, setPayload] = useState({});

  const [stocksList, setStocksList] = useState([]);

  React.useEffect(() => {
    axiosRequest
      .get("stocks/")
      .then((response) => {
        setStocksList(response.data);
        localStorage.setItem("stocksList", JSON.stringify(response.data));
      })
      .catch((error) => {
        console.error("There was an error!", error);
        notification.error({
          message: "Error",
          description: "There was an error fetching the stock list!",
        });
      });
  }, []);

  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };

  const updatePayload = (evt) => {
    setPayload((prevPayload) => ({
      ...prevPayload,
      [evt.target.id]: evt.target.value,
    }));
  };

  const [post, setPost] = React.useState(null);

  function createPost() {
    axiosRequest
      .post("stocks/", payload)
      .then((response) => {
        setPost(response.data);
        notification.success({
          message: "Success",
          description: "Post created successfully!",
        });
      })
      .catch((error) => {
        console.error("There was an error creating the post!", error);
        // Handle the error appropriately here (e.g., show an error message to the user)
        notification.error({
          message: "Error",
          description: "There was an error creating the post!",
        });
      });
  }

  const handleSubmit = () => {
    console.log("Submit", payload);
    createPost();
    onClose();
  };

  return (
    <div>
      <h1>Welcome to the Stock Price Website</h1>
      {/* Add your components and content here */}
      <div>
        <Row gutter={16}>
          <Col span={12}>
            <Card bordered={false}>
              <Statistic
                title="Active"
                value={11.28}
                precision={2}
                valueStyle={{
                  color: "#3f8600",
                }}
                prefix={<ArrowUpOutlined />}
                suffix="%"
              />
            </Card>
          </Col>
          <Col span={12}>
            <Card bordered={false}>
              <Statistic
                title="Idle"
                value={9.3}
                precision={2}
                valueStyle={{
                  color: "#cf1322",
                }}
                prefix={<ArrowDownOutlined />}
                suffix="%"
              />
            </Card>
          </Col>
        </Row>
      </div>
      <FloatButton onClick={showDrawer} icon={<PlusOutlined />} />

      <Drawer
        title="Add a new stock"
        width={720}
        onClose={onClose}
        open={open}
        styles={{
          body: {
            paddingBottom: 80,
          },
        }}
        extra={
          <Space>
            <Button size="small" shape="round" danger onClick={onClose}>
              Cancel
            </Button>
            <Button
              onClick={handleSubmit}
              type="primary"
              size="small"
              shape="round"
            >
              Submit
            </Button>
          </Space>
        }
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="stock_code"
                label="Code"
                rules={[
                  {
                    required: true,
                    message: "Please enter user name",
                  },
                ]}
                onChange={updatePayload}
              >
                <Input placeholder="Please enter user name" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="stock_name"
                label="Name"
                rules={[
                  {
                    required: true,
                    message: "Please select an owner",
                  },
                ]}
                onChange={updatePayload}
              >
                <Input placeholder="Please enter user name" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </div>
  );
};

export default HomePage;
