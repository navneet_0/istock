import React, { useState, useEffect } from "react";
import { Table, notification, Button, Space, Modal, Form, Input, Typography, Select, Row, Col } from "antd";
import { PlusOutlined, EditOutlined } from '@ant-design/icons';
import _ from 'lodash';
import axiosRequest from "../utils/axiosRequest";

const { Paragraph } = Typography;

const columns = (updateStock) => [
  {
    title: "Scrip",
    render: (text, record) => (
      <div>
        <Paragraph copyable>{record.stock_code}</Paragraph>
      </div>
    ),
    key: 'stock_code',
    sorter: (a, b) => a.stock_code.localeCompare(b.stock_code),
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: "Name",
    render: (text, record) => (
      <div>
        <p>{record.stock_name}</p>
      </div>
    ),
  },
  {
    title: "Name number",
    render: (text, record) => (
      <div>
        <Space size="large">
          {record.unity_root}
          <EditOutlined
            onClick={() => {
              let updatedData = {
                stock_code: record.stock_code,
                stock_name: record.stock_name,
              };
              Modal.confirm({
                title: 'Edit Stock',
                content: (
                  <Form
                    name='editStockForm'
                    initialValues={{
                      stock_code: record.stock_code,
                      stock_name: record.stock_name,
                    }}
                    onFinish={(values) => updateStock(values)}
                  >
                    <Form.Item
                      label='Stock Code'
                      name='stock_code'
                      rules={[{ required: true, message: 'Please input the stock code!' }]}
                    >
                      <Input disabled={true} />
                    </Form.Item>
                    <Form.Item
                      label='Stock Name'
                      name='stock_name'
                      rules={[{ required: true, message: 'Please input the stock name!' }]}
                      onChange={(e) => {
                        updatedData.stock_name = e.target.value;
                      }}
                    >
                      <Input />
                    </Form.Item>
                  </Form>
                ),
                onOk() {
                  updateStock(updatedData);
                }
              });
            }}
          />
        </Space>
      </div>
    ),
  }
];

const StockList = () => {
  const [stockListData, setListData] = useState(null);

  useEffect(() => {
    axiosRequest.get('stocks/')
      .then((response) => {
        setListData(response.data);
      })
      .catch((error) => {
        notification.error({
          message: 'Error',
          description: 'There was an error fetching the stock list!',
        });
      });
  }, []);

  const updateStock = (values) => {
    axiosRequest.put('stocks/' + values.stock_code + '/', values)
      .then((response) => {
        notification.success({
          message: 'Success',
          description: 'Stock updated successfully!',
        });
      })
      .catch((error) => {
        notification.error({
          message: 'Error',
          description: 'There was an error updating the stock!',
        });
      });
  };

  if (!stockListData) return null;

  return (
    <div>
      <Row justify="center" gutter={[16, 16]}>
        <Col xs={24} sm={12} md={8} lg={6}>
          <h3>Stocks List</h3>
        </Col>
        <Col xs={24} sm={12} md={8} lg={6}>
          <Select
            style={{ width: '100%' }}
            showSearch
            placeholder="Select stock"
            optionFilterProp="label"
            filterSort={(optionA, optionB) =>
              (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
            }
          >
            {!_.isEmpty(stockListData) && stockListData.map((stock) => (
              <Select.Option key={stock.stock_code} value={stock.stock_code} label={stock.stock_code}>
                {stock.stock_name}
              </Select.Option>
            ))}
          </Select>
        </Col>
        <Col xs={24} sm={12} md={8} lg={6}>
          <Button type="primary" icon={<PlusOutlined />}>Search</Button>
        </Col>
      </Row>
      <Row justify="center" gutter={[16, 16]}>
        <Col xs={24}>
          <Table
            dataSource={stockListData}
            columns={columns(updateStock)}
            size='small'
            scroll={{ x: 'max-content' }}
          />
        </Col>
      </Row>
    </div>
  );
};

export default StockList;