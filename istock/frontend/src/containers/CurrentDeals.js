import React, { useState, state, useEffect } from "react";
import { Table, notification, Collapse, Form, Select, FloatButton, Drawer, Space, Button, Row, Col, Input } from "antd";
import {
    PlusOutlined,
    ArrowDownOutlined,
    ArrowUpOutlined,
  } from "@ant-design/icons";
import { Column } from "@ant-design/plots";
import axiosRequest from "../utils/axiosRequest";
import _ from "lodash";

import useGetStocksList from "../components/customHooks.js/useGetStocksList";

const { Option } = Select;

const CurrrentDeals = () => {

    const [open, setOpen] = useState(false);

  const stocksList = useGetStocksList();
  const [stockData, setData] = useState(null);
  const [stockCode, setStockCode] = useState("");

  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };



  useEffect(() => {
    axiosRequest
      .get("transactions/?stock_code=CELLO")
      .then((response) => {
        setData(response.data);
        if (response.data && response.data.length > 0) {
          setStockCode(response.data[0].stock);
        }
      })
      .catch((error) => {
        console.error("There was an error!", error);
        notification.error({
          message: "Error",
          description: "There was an error fetching the stock list!",
        });
      });
  }, []);

  const config = {
    data: stockData,
    xField: "trade_date",
    yField: "volume_traded",
  };

  return (
    <div>
      <h3>Current Deals</h3>

      <div>

      <FloatButton onClick={showDrawer} icon={<PlusOutlined />} />



      </div>

      <Drawer
        title="Add a new stock"
        width={720}
        onClose={onClose}
        open={open}
        styles={{
          body: {
            paddingBottom: 80,
          },
        }}
        extra={
          <Space>
            <Button size="small" shape="round" danger onClick={onClose}>
              Cancel
            </Button>
            <Button
            //   onClick={handleSubmit}
              type="primary"
              size="small"
              shape="round"
            >
              Submit
            </Button>
          </Space>
        }
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="stock_code"
                label="Code"
                rules={[
                  {
                    required: true,
                    message: "Please enter user name",
                  },
                ]}
                // onChange={updatePayload}
              >
                <Input placeholder="Please enter stock scrip" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="stock_name"
                label="Name"
                rules={[
                  {
                    required: true,
                    message: "Please select an owner",
                  },
                ]}
                // onChange={updatePayload}
              >
                <Input placeholder="Please enter user name" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </div>
  );
};

export default CurrrentDeals;


