import React from "react";
import { Table, notification, Select, DatePicker, Space, Typography, Badge } from "antd";
import dayjs from "dayjs";

import _ from "lodash";
import customParseFormat from "dayjs/plugin/customParseFormat";

import "../components/customCSS/TableStyle.css";
import axiosRequest from "../utils/axiosRequest";

import useGetStocksList from "../components/customHooks.js/useGetStocksList";
import { formatNumberWithCommas } from "../components/UtilFunctions";

dayjs.extend(customParseFormat);
const dateFormat = "DD-MM-YYYY";
const { Option } = Select;
const { RangePicker } = DatePicker;
const { Text } = Typography;

const displayDevRatio = (value) => {
  if (value > 61.8) {
    return (
      <span style={{ color: "green", fontWeight: "bolder" }}>
        <Badge
          className="site-badge-count-109"
          count={value}
          style={{
            backgroundColor: '#52c41a',
          }}
        />
      </span>
    );
  }
  return <span style={{ color: "black" }}>{value}</span>;
};

const displayVolTradeRatio = (value) => {
  if (value > 1.618) {
    return (
      // <span ><Text code
      // style={{ color: "blue", fontWeight: "bolder" }}
      // >{value}</Text></span>
      <Badge
        className="site-badge-count-109"
        count={value}
        style={{
          backgroundColor: '#52c41a',
        }}
      />
    );
  }
  return <span style={{ color: "black" }}>{value}</span>;
};

const highLightColumn = (record) => {
  if (record.volume_traded_ratio >= 1.618) {
    return "greenyellow";
  }
  return "white";
};



const columns = [
  {
    title: "Scrip",
    render: (text, record) => (
      <div>
        <p>{record.stock}</p>
      </div>
    ),
  },
  {
    title: "Date",
    render: (text, record) => (
      <div>
        <p>{dayjs(record.trade_date).format(dateFormat)}</p>
      </div>
    ),
  },
  {
    title: "Prev Close",
    render: (text, record) => (
      <div>
        <p>{record.previous_close}</p>
      </div>
    ),

  },
  {
    title: "Open",
    render: (text, record) => (
      <div>
        <p>{record.day_open}</p>
        <p style={{ fontSize: "9pt" }}>{record.opening_price_unity_root}</p>
      </div>
    ),
  },
  {
    title: "High",
    render: (text, record) => (
      <div>
        <p>{record.day_high}</p>
        <p style={{ fontSize: "9pt" }}>{record.high_price_unity_root}</p>
      </div>
    ),
  },
  {
    title: "Low",
    render: (text, record) => (
      <div>
        <p>{record.day_low}</p>
        <p style={{ fontSize: "9pt" }}>{record.low_price_unity_root}</p>

      </div>
    ),
  },
  {
    title: "Close",
    render: (text, record) => (
      <div>
        <p>{record.day_close}</p>
        <p style={{ fontSize: "9pt" }}>{record.close_price_unity_root}</p>
      </div>
    ),
  },
  {
    title: "VWAP",
    render: (text, record) => (
      <div>
        <p>{record.vwap}</p>
      </div>
    ),

  },
  {
    title: "Trade Volume",
    render: (text, record) => (
      <div>
        <p>{formatNumberWithCommas(record.volume_traded)}</p>
        {/* show summary in K or Lacs under volume_traded */}


      </div>
    ),
  },
  {
    title: "Delv Vol",
    render: (text, record) => (
      <div>
        <p>{formatNumberWithCommas(record.volume_delivered)}</p>
      </div>
    ),
  },
  // {
  //   title: "Trade Value",
  //   render: (text, record) => (
  //     <div>
  //       <p>{formatNumberWithCommas(record.traded_value)}</p>
  //     </div>
  //   ),
  // },
  {
    title: "Deliv Ratio",
    render: (text, record) => (
      <div>
        <p>{displayDevRatio(record.ratio_delv_to_traded)}</p>
      </div>
    ),
  },
  {
    title: "Trade Vol % yesterday",
    render: (text, record) => (
      <div
      // style= {{backgroundColor: highLightColumn(record)}}
      >
        <p>{displayVolTradeRatio(record.volume_traded_ratio)}</p>
      </div>
    ),
  },
  {
    title: "Delv Vol % yesterday",
    render: (text, record) => (
      <div
      // style= {{backgroundColor: highLightColumn(record)}}
      >
        <p>{displayVolTradeRatio(record.volume_delivered_ratio)}</p>
      </div>
    ),
  },
  {
    title: "Gap",
    render: (text, record) => (
      <div
      // style= {{backgroundColor: highLightColumn(record)}}
      >
        <p>{(record.gap_opening ? record.gap_opening : null)}</p>
      </div>
    ),
  },
  {
    title: "Week",
    render: (text, record) => (
      <div
      // style= {{backgroundColor: highLightColumn(record)}}
      >
        <p>{record.current_week}</p>
      </div>
    ),
  },
];

const DailyTransactions = () => {
  const [stockData, setData] = React.useState([]);
  const [filteredData, setFilteredData] = React.useState([]);
  // const [stockCode, setStockCode] = React.useState("CELLO");
  const [dateRange, setDateRange] = React.useState([null, null]);
  const stocksList = useGetStocksList();
  let stockCode = localStorage.getItem("stockCode") || "CELLO";
  const stocksListData = useGetStocksList();

  // Function to get start and end dates for current month
  const getCurrentMonthDates = () => {
    const startOfMonth = dayjs().startOf("month");
    const endOfMonth = dayjs().endOf("month");
    return [startOfMonth, endOfMonth];
  };

  const fetchData = (stockCode, startDate, endDate) => {
    stockCode = localStorage.getItem("stockCode") || "CELLO";
    let query = `transactions/?stock_code=${stockCode}`;
    if (startDate && endDate) {
      query += `&search_trade_date__gte=${startDate.format(
        "YYYY-MM-DD"
      )}&search_trade_date__lte=${endDate.format("YYYY-MM-DD")}`;
    }
    axiosRequest
      .get(query)
      .then((response) => {
        setData(response.data);
        setFilteredData(response.data);
      })
      .catch((error) => {
        console.error("There was an error!", error);
        notification.error({
          message: "Error",
          description: "There was an error fetching the stock list!",
        });
      });
  };

  React.useEffect(() => {
    const [startOfMonth, endOfMonth] = getCurrentMonthDates();
    setDateRange([startOfMonth, endOfMonth]);
    fetchData(stockCode, startOfMonth, endOfMonth);
  }, []);

  const handleStockCodeChange = (value) => {
    // setStockCode(value);
    //store value of selected stock in local storage
    localStorage.setItem("stockCode", value);
    fetchData(value, dateRange[0], dateRange[1]);
  };

  const handleDateRangeChange = (dates) => {
    setDateRange(dates);
    fetchData(stockCode, dates[0], dates[1]);
  };

  return (
    <div>
      <h3>Daily Transactions data {stockCode}</h3>
      <div style={{ marginBottom: 16 }}>
        <Space direction="horizontal" size={12}>
          <Select
            value={stockCode}
            style={{ width: 220 }}
            showSearch
            placeholder="Select stock"
            optionFilterProp="label"
            filterSort={(optionA, optionB) =>
              (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
            }
            onChange={handleStockCodeChange}
          >
            {!_.isEmpty(stocksListData) && stocksListData.map((stock) => (
              <Select.Option key={stock.stock_code} value={stock.stock_code} label={stock.stock_code}>
                {stock.stock_name}
              </Select.Option>
            ))}
          </Select>
          <RangePicker
            format={dateFormat}
            onChange={handleDateRangeChange}
            defaultValue={dateRange}
          />
        </Space>
      </div>
      <Table
        dataSource={filteredData}
        columns={columns}
        size="small"
        rowClassName={(record) =>
          record.ratio_delv_to_traded >= 61.8 ? "highlight" : "normal"
        }
      />
    </div>
  );
};

export default DailyTransactions;
