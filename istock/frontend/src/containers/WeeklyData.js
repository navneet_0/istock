import React, { useState } from 'react';
import { Table, Select, notification, Tooltip, Badge } from 'antd';
import { BidirectionalBar, Stock } from '@ant-design/plots';
import _ from 'lodash';
import axiosRequest from '../utils/axiosRequest';
// import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import dayjs from 'dayjs';

import useGetStocksList from "../components/customHooks.js/useGetStocksList";
import { formatNumberWithCommas, displayVolTradeRatio, displayDevRatio } from "../components/UtilFunctions";

const { Option } = Select;

const WeeklyData = () => {
  const [stockCode, setStockCode] = useState('');
  const [weeklyData, setWeeklyData] = useState([]);
  const stocksListData = useGetStocksList();

  const columns = [
    {
      title: "Scrip",
      render: (text, record) => (
        <div>
          <p>{record.stock}</p>
        </div>
      ),
    },
    {
      title: "Week Number",
      render: (text, record) => (
        <div>
          <Tooltip title={() => <span>{dayjs(record.week_start).format('DD-MM-YYYY')}
            <br />
            {dayjs(record.week_end).format('DD-MM-YYYY')}
          </span>}>
            <p>{record.week_number}</p>
          </Tooltip>
        </div>
      ),

    },
    {
      title: "Open",
      render: (text, record) => (
        <div>
          <p>{record.week_open}</p>

        </div>
      ),
    },
    {
      title: "High",
      render: (text, record) => (
        <div>
          <p>{record.week_high}</p>
          <p style={{ fontSize: "9pt" }}>{record.high_price_unity_root}</p>
        </div>
      ),
    },
    {
      title: "Low",
      render: (text, record) => (
        <div>
          <p>{record.week_low}</p>
          <p style={{ fontSize: "9pt" }}>{record.low_price_unity_root}</p>

        </div>
      ),
    },
    {
      title: "Close",
      render: (text, record) => (
        <div>
          <p>{record.week_close}</p>
          <p style={{ fontSize: "9pt" }}>{record.close_price_unity_root}</p>
        </div>
      ),
    },
    {
      title: "VWAP",
      render: (text, record) => (
        <div>
          <p>{record.week_vwap}</p>
        </div>
      ),

    },
    {
      title: "Gap", render: (text, record) => (<div>
        {/* <p>{record.gap_opening}</p> */}
        <p>{record.week_price_range_ratios.upper_wick_ratio}</p>
        {record.week_price_range_ratios.real_body_ratio}
        <p>{record.week_price_range_ratios.lower_wick_ratio}</p>


      </div >),

    },
    {
      title: "Trade Volume",
      render: (text, record) => (
        <div>
          <p>{formatNumberWithCommas(record.week_volume)}</p>
          {record.week_volume}
          {/* show summary in K or Lacs under volume_traded */}


        </div>
      ),
    },
    {
      title: "Delv Vol",
      render: (text, record) => (
        <div>
          <p>{formatNumberWithCommas(record.week_delivered)}</p>
        </div>
      ),
    },
    {
      title: "Trade Value",
      render: (text, record) => (
        <div>
          <p>{formatNumberWithCommas(record.week_traded_value)}</p>
        </div>
      ),
    },
    {
      title: "Deliv Ratio",
      render: (text, record) => (
        <div>
          <p>{displayDevRatio(record.ratio_delv_to_traded)}</p>
        </div>
      ),
    },
    {
      title: "Trade Vol % yesterday",
      render: (text, record) => (
        <div
        // style= {{backgroundColor: highLightColumn(record)}}
        >
          <p>{displayVolTradeRatio(record.volume_traded_ratio)}</p>
        </div>
      ),
    },
    {
      title: "Delv Vol % yesterday",
      render: (text, record) => (
        <div
        // style= {{backgroundColor: highLightColumn(record)}}
        >
          <p>{displayVolTradeRatio(record.volume_delivered_ratio)}</p>
        </div>
      ),
    },
    {
      title: "Gap",
      render: (text, record) => (
        <div
        // style= {{backgroundColor: highLightColumn(record)}}
        >
          <p>{(record.gap_opening ? record.gap_opening : null)}</p>
        </div>
      ),
    },

  ];

  const handleStockCodeChange = (value) => {
    setStockCode(value);
  };

  // const filteredData = stockCode ? data.map((item) => ({ week: item.week, [stockCode]: item[stockCode] })) : data;

  const config = {
    data: weeklyData,
    xField: 'week_number',
    layout: 'vertical',

    style: {
      fill: (d) => {
        if (d.groupKey === 'week_volume') return '#64DAAB';
        return '#6395FA';
      },
    },
    yField: ['week_volume', 'week_delivered'],
  };

  const stockConfig = {
    xField: 'week_number',
    yField: ['week_open', 'week_close', 'week_high', 'week_low'],
    fallingFill: 'red',
    risingFill: 'green',
    data: weeklyData.map((i) => ({ ...i, date: new Date(i.week_start) })),
  };

  return (
    <div>
      <h1>Weekly Data</h1>
      <Select
        defaultValue="CELLO"
        style={{ width: 220 }}
        showSearch
        placeholder="Select stock"
        optionFilterProp="label"
        filterSort={(optionA, optionB) =>
          (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
        }
        onChange={(value) => {
          axiosRequest
            .get(`weekly/?stock_code=${value}`)
            .then((response) => {
              setWeeklyData(response.data);
              if (response.data && response.data.length > 0) {
                setStockCode(response.data[0].stock);
                console.log("data", weeklyData);
              }
            })
            .catch((error) => {
              console.error("There was an error!", error);
              notification.error({
                message: "Error",
                description: "There was an error fetching the stock list!",
              });
            });
        }}
      >
        {!_.isEmpty(stocksListData) && stocksListData.map((stock) => (
          <Select.Option key={stock.stock_code} value={stock.stock_code} label={stock.stock_code}>
            {stock.stock_name}
          </Select.Option>
        ))}


      </Select>

      <Stock {...stockConfig} />

      <BidirectionalBar {...config} />
      <Table dataSource={weeklyData} columns={columns} />

    </div>
  );
};

export default WeeklyData;