import React, { useState, state, useEffect } from "react";
import { Table, notification, Collapse, Form, Select } from "antd";
import { Column } from "@ant-design/plots";
import axiosRequest from "../utils/axiosRequest";
import _ from "lodash";

import useGetStocksList from "../components/customHooks.js/useGetStocksList";

const { Option } = Select;

const VolumeAnalysis = () => {
  const stocksListData = useGetStocksList();
  const [stockData, setData] = useState(null);
  const [stockCode, setStockCode] = useState("");

  useEffect(() => {
    axiosRequest
      .get("transactions/?stock_code=CELLO")
      .then((response) => {
        const sortedData = _.orderBy(response.data, ['trade_date'], ['desc']);
        setData(sortedData);
        if (sortedData && sortedData.length > 0) {
          setStockCode(sortedData[0].stock);
        }
      })
      .catch((error) => {
        console.error("There was an error!", error);
        notification.error({
          message: "Error",
          description: "There was an error fetching the stock list!",
        });
      });
  }, []);

  const monthColorMapping = {
    "01": "#FF5733", // January - Red
    "02": "#FFBD33", // February - Yellow
    "03": "#75FF33", // March - Green
    "04": "#33FF57", // April - Light Green
    "05": "#33FFBD", // May - Cyan
    "06": "#33D4FF", // June - Light Blue
    "07": "#3375FF", // July - Blue
    "08": "#5733FF", // August - Dark Blue
    "09": "#BD33FF", // September - Purple
    "10": "#FF33D4", // October - Pink
    "11": "#FF3375", // November - Rose
    "12": "#FF3333", // December - Dark Red
  };

  const getColorByMonth = (trade_date) => {
    const month = new Date(trade_date).toISOString().slice(5, 7);
    return monthColorMapping[month];
  };

  const config = {
    data: stockData,
    xField: "trade_date",
    yField: "volume_traded",
    color: ({ trade_date }) => getColorByMonth(trade_date),
  };

  const stackConfig = {
    data: stockData,
    xField: "trade_date",
    yField: "volume_traded",
    color: ({ trade_date }) => getColorByMonth(trade_date),
    stack: true,
    colorField: 'type',
    label: {
      text: 'stock',
      textBaseline: 'bottom',
      position: 'inside',
    },
  };

  return (
    <div>
      <h3>Daily Volume Analysis {stockCode !== "" ? stockCode : null}</h3>
      <Collapse>
        <Select
          //default value should check for stockCode in local storage else set as CELLO
          defaultValue={localStorage.getItem("stockCode") || "CELLO"}
          // defaultValue= localStorage={} "CELLO"
          style={{ width: 220 }}
          showSearch
          placeholder="Select stock"
          optionFilterProp="label"
          filterSort={(optionA, optionB) =>
            (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
          }
          onChange={(value) => {
            localStorage.setItem("stockCode", value);
            axiosRequest
              .get(`transactions/?stock_code=${value}`)
              .then((response) => {
                const sortedData = _.orderBy(response.data, ['trade_date'], ['desc']);
                setData(sortedData);
                if (sortedData && sortedData.length > 0) {
                  setStockCode(sortedData[0].stock);
                }
              })
              .catch((error) => {
                console.error("There was an error!", error);
                notification.error({
                  message: "Error",
                  description: "There was an error fetching the stock list!",
                });
              });
          }}
        >
          {!_.isEmpty(stocksListData) && stocksListData.map((stock) => (
            <Select.Option key={stock.stock_code} value={stock.stock_code} label={stock.stock_code}>
              {stock.stock_name}
            </Select.Option>
          ))}


        </Select>

        {/* <Form>
          <Form.Item label="Stock Code">
            <input type="text" name="stock_code" />
          </Form.Item>
        </Form> */}
      </Collapse>
      {stockData && <Column {...config} />}
    </div>
  );
};

export default VolumeAnalysis;