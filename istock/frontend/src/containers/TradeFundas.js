import React from 'react';

const TradeFundas = () => {
    return (
        <div>
            <h1>Trade Fundas</h1>
            
            <p>Fib ratios in volumes traded/delivery qty</p>
            <p>Based on Gap opening up or down</p>
            <p>Check for 3,6,9 rule in price, volume, delivery</p>
        </div>
    );
};

export default TradeFundas;