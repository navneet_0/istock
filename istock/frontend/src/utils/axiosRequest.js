import axios from "axios";


const instance = axios.create({
    // baseURL: process.env.APP_PUBLIC_API_URL,
    baseURL: 'http://127.0.0.1:9100/api/v0/',
    timeout: 1000,
    // headers: {'X-Custom-Header': 'oyelucky'}
  });


  instance.interceptors.request.use(
    (config) => {
        // config.headers["X-Server"] = 'oyelucky';
      return config;
    },
    (error) => {
        console.log('Axios Request error', error);
        Promise.reject(error)
    }
  );

export default instance;