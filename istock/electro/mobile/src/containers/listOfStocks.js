import { useState, useEffect } from 'react';
import { Table, Divider, FloatButton, Modal, Form, Typography } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import api from '../utils/api';

const { Paragraph, Text } = Typography;

const columns = [
    {
    title: "Scrip",
    render: (text, record) => (
      <div>
        {/*<Paragraph copyable></Paragraph>*/}
          {record.stock_code}
      </div>
    ),
    key: 'stock_code',
    sorter: (a, b) => a.stock_code.localeCompare(b.stock_code),

    sortDirections: ['descend', 'ascend'],

  },
  {
    title: "Name",
    render: (text, record) => (
      <div>
        <p>{record.stock_name}</p>

      </div>
    ),
  },
];

export function ListOfStocks() {
    const [data, setData] = useState([]);

    useEffect(() => {
        api.get('/stocks/')
            .then(response => {
                setData(response.data);
            })
            .catch(error => {
                console.error('Error fetching stock data:', error);
            });
    }, []);

    return (
        <div>
            <Divider />
            <Table columns={columns} dataSource={data} />
            <FloatButton icon={<PlusOutlined />}
                onClick={() => Modal.confirm({
                    title: 'Add a new stock',
                    content:
                        <div>
                            <Form>
                                <Form.Item label="Stock Name">
                                    <input type="text" />
                                </Form.Item>
                                <Form.Item label="Stock Symbol">
                                    <input type="text" />
                                </Form.Item>
                            </Form>
                        </div>,
                    okText: 'Add',
                    cancelText: 'Cancel',
                    onOk() {
                        console.log('OK');
                    },
                    onCancel() {
                        console.log('Cancel');
                    },
                })}
            />
        </div>
    )
}