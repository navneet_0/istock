from rest_framework import routers

from api.v0.views.istock import StockDetailsViewSet, StockViewSet
from api.v0.views.transactions import DailyTransactionsViewSet, WeeklyDataViewSet

router = routers.SimpleRouter()
router.register(r'details', StockDetailsViewSet, basename='details')
router.register(r'stocks', StockViewSet , basename='stocks')
router.register(r'transactions', DailyTransactionsViewSet, basename='transactions')
router.register(r'weekly', WeeklyDataViewSet, basename='weekly')
# urlpatterns = router.urls