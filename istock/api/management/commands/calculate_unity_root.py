from django.core.management.base import BaseCommand, CommandError

from api.v0.utils.unity_root import get_unity_number_for_scrip

class Command(BaseCommand):
    args = '<name>'
    help = "Get unity root for name based on chaldean numerology"

    def add_arguments(self, parser):
        parser.add_argument("name", type=str)

    def handle(self, *args, **options):
        name = options["name"]
        print(f"Get unity root for name {name}")
        unity_number = get_unity_number_for_scrip(name)
