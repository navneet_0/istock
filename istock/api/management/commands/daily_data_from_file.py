import csv

from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from stocks.models import DailyTransactions, Stock

from api.v0.utils.constants import EQUITY

class Command(BaseCommand):
    args = '<file_path>'
    help = "Upload daily transaction data for stock"

    def add_arguments(self, parser):
        parser.add_argument("file_path", type=str)

    def handle(self, *args, **options):
        file_path = options["file_path"]
        print(f"Upload data from {file_path}")
        record_count = 0
        new_rec = 0
        updated_rec = 0
        # read csv data
        with open(file_path, newline='', encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile)
            header = next(csvreader)  # Read the header row
            # print('Header:', header)

            for row in csvreader:
                '''
                Symbol  	Series  	Date  	Prev Close  	Open Price  	High Price  	Low Price  	Last Price  	Close Price  	Average Price 	Total Traded Quantity  	Turnover ₹  	No. of Trades  	Deliverable Qty  	% Dly Qt to Traded Qty  
CELLO	EQ	06-Nov-2023	648	829	837.4	782	792.15	791.7	806.13	1,79,47,726	14,46,81,63,370.25	4,29,438	1,23,12,977	68.6

                '''
                record_count += 1
                scrip = row[0]
                type = row[1]
                trade_date = row[2]
                previous_close = float(row[3].replace(',', ''))
                day_open = float(row[4].replace(',', ''))  # Remove comma from the number
                day_high = float(row[5].replace(',', ''))
                day_low = float(row[6].replace(',', ''))
                day_close = float(row[8].replace(',', ''))
                vwap = row[9].replace(',', '')
                volume_traded = row[10].replace(',', '')
                volume_delivered = row[13].replace(',', '')
                traded_value = row[11].replace(',', '')
                ratio_delv_to_traded = row[14].replace(',', '')

                # Convert the date format from DD-MMM-YYYY to YYYY-MM-DD
                try:
                    trade_date_obj = datetime.strptime(trade_date, '%d-%b-%Y')
                    trade_date = trade_date_obj.strftime('%Y-%m-%d')
                except ValueError as e:
                    print(f"Date conversion error for {trade_date}: {e}")
                    continue

                print(scrip, type, trade_date, day_open, day_high, day_low, day_close, volume_traded, volume_delivered, traded_value, ratio_delv_to_traded)
                if record_count == 1:
                    try:
                        stock_obj, created = Stock.objects.get_or_create(stock_code=scrip)
                        print(f"Upload data for stock {stock_obj}")
                    except Exception as e:
                        print(f"Stock creation error {scrip} does not exist")
                        continue
                if type == EQUITY:
                    daily_transaction, created = DailyTransactions.objects.get_or_create(
                        stock=stock_obj,
                        trade_date=trade_date,
                        defaults={
                            'previous_close': previous_close,
                            'day_open': day_open,
                            'day_high': day_high,
                            'day_low': day_low,
                            'day_close': day_close,
                            'vwap': vwap,
                            'volume_traded': volume_traded,
                            'volume_delivered': volume_delivered,
                            'traded_value': traded_value,
                            'ratio_delv_to_traded': ratio_delv_to_traded
                        }
                    )
                    if created:
                        new_rec += 1
                    else:
                        updated_rec += 1

        print(f"Total records uploaded {record_count} New records {new_rec} Updated records {updated_rec}")


