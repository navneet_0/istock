import requests
import math
import random
from time import sleep
from datetime import datetime
from django.core.management.base import BaseCommand
from django.utils import timezone

from stocks.models import Stock, DailyTransactions

from api.v0.utils.stock_data import get_adjusted_headers, fetch_data_from_nse

class Command(BaseCommand):
    args = '<stock_code>'
    help = "Fetch transaction data from NSE for stock"

    def add_arguments(self, parser):
        parser.add_argument("stock_code", type=str, nargs='?', default='', help="Stock code for which data needs to be fetched")

    def handle(self, *args, **options):
        stock_code = options['stock_code'].upper() if options['stock_code'] != '' else ''
        stocks_list = Stock.objects.all() if options['stock_code'] == '' else Stock.objects.filter(stock_code=stock_code)
        for stock in stocks_list:
            #delay next call for a random sec between 3 and 9
            delay_time = random.uniform(3, 9)
            sleep(math.floor(delay_time))
            print(stock.stock_code, delay_time)
            fetch_data_from_nse(stock.stock_code)