from django.core.management.base import BaseCommand
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from django.conf import settings
from stocks.models import DailyTransactions
from api.v0.serializers.transactions import DailyTransactionsSerializer
from django.core.paginator import Paginator
from datetime import datetime, timedelta

class Command(BaseCommand):
    help = "Update data from daily transactions for all the stocks to elasticsearch index in localhost"

    def handle(self, *args, **options):
        dailydata_index = 'dailydata'
        es = Elasticsearch([{'host': settings.ELASTICSEARCH_HOST, 'port': settings.ELASTICSEARCH_PORT, 'scheme': 'http'}])

        # Delete index data
        es.delete_by_query(index=dailydata_index, body={"query": {"match_all": {}}})

        # Assuming there's a model or method to fetch the last run timestamp
        # last_run = self.get_last_run_timestamp()
        current_run = datetime.now()

        # Fetch transactions updated since the last run
        daily_transactions = DailyTransactions.objects.all()
        daily_transactions_serializer = DailyTransactionsSerializer(daily_transactions, many=True)
        daily_transactions_data = daily_transactions_serializer.data

        paginator = Paginator(daily_transactions_data, 1000)
        for page in range(1, paginator.num_pages + 1):
            daily_transactions_data_page = paginator.page(page)
            bulk_data = []
            for data in daily_transactions_data_page:
                data_dict = data
                trade_date = datetime.strptime(data_dict['trade_date'], '%Y-%m-%d').date()
                data_dict['trade_date'] = trade_date
                data_dict['year'] = trade_date.year  # Add year field
                data_dict['month'] = trade_date.month
                bulk_data.append({
                    "_index": dailydata_index,
                    "_id": data_dict['id'],
                    "_source": data_dict
                })
            bulk(es, bulk_data)

        # Update the last run timestamp after successful run
        # self.update_last_run_timestamp(current_run)

        print(f"Data updated to elasticsearch index\t{dailydata_index}")

    def get_last_run_timestamp(self):
        # Implement logic to retrieve the last run timestamp
        # Placeholder return
        return datetime.now() - timedelta(days=1)

    def update_last_run_timestamp(self, timestamp):
        # Implement logic to update the last run timestamp
        pass