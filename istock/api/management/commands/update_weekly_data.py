import datetime

from django.core.management.base import BaseCommand
from django.db.models import Avg, Sum

from stocks.models import Stock, DailyTransactions, WeeklyData


class Command(BaseCommand):
    args = '<stock_code>'
    help = "Update weekly data for stocks"


    def add_arguments(self, parser):
        parser.add_argument("stock_code", type=str, nargs='?', default='', help="Stock code for which weekly data is to be updated")

    def handle(self, *args, **options):
        # Get all stocks
        stocks = Stock.objects.all()

        for stock in stocks:
            current_week = datetime.datetime.now().isocalendar()[1]
            current_year = datetime.datetime.now().year
            # First check if data for stock exists in WeeklyData table
            latest_weekly_data = WeeklyData.objects.filter(stock=stock).order_by('-week_number').first()
            latest_week = latest_weekly_data.week_number if latest_weekly_data else 1

            print(f"Latest week for {stock.stock_code} is {latest_week} and current week is {current_week}")

            if latest_week == current_week:
                # update data for current week if record exists else create new record for current week
                weekly_data = WeeklyData.objects.get(stock=stock, week_number=current_week, year=current_year)
                week_transactions_data = DailyTransactions.objects.filter(stock=stock, trade_date__week=current_week,
                                                                          trade_date__year=current_year)
                if weekly_data:
                    weekly_data.week_open = week_transactions_data.order_by('trade_date').first().day_open
                    weekly_data.week_close = week_transactions_data.order_by('trade_date').last().day_close
                    weekly_data.week_high = week_transactions_data.order_by('day_high').last().day_high
                    weekly_data.week_low = week_transactions_data.order_by('day_low').first().day_low
                    weekly_data.week_vwap = week_transactions_data.aggregate(Avg('vwap'))['vwap__avg']
                    weekly_data.week_volume = week_transactions_data.aggregate(Sum('volume_traded'))['volume_traded__sum']
                    weekly_data.week_delivered = week_transactions_data.aggregate(Sum('volume_delivered'))['volume_delivered__sum']
                    weekly_data.week_traded_value = week_transactions_data.aggregate(Sum('traded_value'))['traded_value__sum']
                    weekly_data.save()
                    print(f"Updated weekly data for {stock.stock_code} for week {current_week} of year {current_year}")
                else:
                    WeeklyData.objects.create(stock=stock, week_number=current_week, year=current_year,
                                                week_start=week_transactions_data.order_by('trade_date').first().trade_date,
                                                week_end=week_transactions_data.order_by('trade_date').last().trade_date,
                                                week_open=week_transactions_data.order_by('trade_date').first().day_open,
                                                week_close=week_transactions_data.order_by('trade_date').last().day_close,
                                                week_high=week_transactions_data.order_by('day_high').last().day_high,
                                                week_low=week_transactions_data.order_by('day_low').first().day_low,
                                                week_vwap=week_transactions_data.aggregate(Avg('vwap'))['vwap__avg'],
                                                week_volume=week_transactions_data.aggregate(Sum('volume_traded'))['volume_traded__sum'],
                                                week_delivered=week_transactions_data.aggregate(Sum('volume_delivered'))['volume_delivered__sum'],
                                                week_traded_value=week_transactions_data.aggregate(Sum('traded_value'))['traded_value__sum'])
                print(f"Created weekly data for {stock.stock_code} for week {current_week} of year {current_year}")

            elif current_week - latest_week >= 1:
                # If the difference between the current week and the latest week is more than 1
                # then we need to create weekly data for the missing weeks
                for week in range(latest_week, current_week+1):
                    print(f"Creating weekly data for {stock.stock_code} for week {week} of year {current_year}")
                    #Get start and end date of week based on latest week value

                    start_date = datetime.date.fromisocalendar(current_year,week,1)
                    end_date = datetime.date.fromisocalendar(current_year,week,5)
                    weekly_data = DailyTransactions.objects.filter(stock=stock, trade_date__gte=start_date, trade_date__lte=end_date)
                    # print(weekly_data)
                    if weekly_data:
                        week_open = weekly_data.order_by('trade_date').first().day_open
                        week_close = weekly_data.order_by('trade_date').last().day_close
                        week_high = weekly_data.order_by('day_high').last().day_high
                        week_low = weekly_data.order_by('day_low').first().day_low
                        week_vwap = weekly_data.aggregate(Avg('vwap'))['vwap__avg']
                        week_volume = weekly_data.aggregate(Sum('volume_traded'))['volume_traded__sum']
                        week_delivered = weekly_data.aggregate(Sum('volume_delivered'))['volume_delivered__sum']
                        week_traded_value = weekly_data.aggregate(Sum('traded_value'))['traded_value__sum']

                        weekly_data, created = WeeklyData.objects.get_or_create(stock=stock, week_number=week, year=current_year,
                                                                       defaults={
                                                                            'week_start': start_date,
                                                                            'week_end': end_date,
                                                                            'week_open': week_open,
                                                                            'week_close': week_close,
                                                                            'week_high': week_high,
                                                                            'week_low': week_low,
                                                                            'week_vwap': week_vwap,
                                                                            'week_volume': week_volume,
                                                                            'week_delivered': week_delivered,
                                                                            'week_traded_value': week_traded_value
                                                                          })
                        if not created:
                            print("Already Created", weekly_data.week_number)
                            weekly_data.week_end = end_date
                            weekly_data.week_close = week_close
                            weekly_data.week_high = week_high
                            weekly_data.week_low = week_low
                            weekly_data.week_vwap = week_vwap
                            weekly_data.week_volume = week_volume
                            weekly_data.week_delivered = week_delivered
                            weekly_data.week_traded_value = week_traded_value
                            weekly_data.save()
                        else:
                            print(f"Created weekly data for {stock.stock_code} for week {week} of year {current_year}")

                        weekly_data.save()
