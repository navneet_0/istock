import requests


from datetime import datetime
from time import sleep
from stocks.models import Stock, DailyTransactions

from api.v0.utils.constants import EQUITY

#https://www.nseindia.com/api/historical/securityArchives?from=01-06-2024&to=01-07-2024&symbol=ZOMATO&dataType=priceVolumeDeliverable&series=ALL

'''Sample response
{'_id': '66167f7fe8b5eef20dd1f151', 'CH_SYMBOL': 'GODREJAGRO', 'CH_SERIES': 'EQ', 'CH_MARKET_TYPE': 'N', 'CH_TRADE_HIGH_PRICE': 542, 'CH_TRADE_LOW_PRICE': 533.1, 'CH_OPENING_PRICE': 540, 'CH_CLOSING_PRICE': 537.35, 'CH_LAST_TRADED_PRICE': 537.7, 'CH_PREVIOUS_CLS_PRICE': 540.95, 'CH_TOT_TRADED_QTY': 61686, 'CH_TOT_TRADED_VAL': 33083859.4, 'CH_52WEEK_HIGH_PRICE': 579, 'CH_52WEEK_LOW_PRICE': 417.15, 'CH_TOTAL_TRADES': 5702, 'CH_ISIN': 'INE850D01014', 'CH_TIMESTAMP': '2024-04-10', 'TIMESTAMP': '2024-04-09T18:30:00.000Z', 'createdAt': '2024-04-10T12:01:03.485Z', 'updatedAt': '2024-04-10T12:01:03.485Z', '__v': 0, 'COP_DELIV_QTY': 30377, 'COP_DELIV_PERC': 49.24, 'VWAP': 536.33, 'mTIMESTAMP': '10-Apr-2024'}
'''

def get_adjusted_headers():
    return {
        'Host': 'www.nseindia.com',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0',
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'X-Requested-With': 'XMLHttpRequest',
        'DNT': '1',
        'Connection': 'keep-alive',
    }


def get_transaction_data_for_stock(stock_code, from_date, to_date):
    url = f"https://www.nseindia.com/api/historical/securityArchives?from={from_date}&to={to_date}&symbol={stock_code}&dataType=priceVolumeDeliverable&series=ALL"
    print(url)
    response = requests.get(url,  timeout=30, headers=get_adjusted_headers())
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error in fetching data for {stock_code} from {from_date} to {to_date} with status code {response.status_code}")
        print(response.text)
    return None


def get_daily_data_for_stocks():
    stocks = Stock.objects.all()
    for stock in stocks:
        stock_code = stock.stock_code
        print(f"Fetching data for {stock_code}")
        sleep(5)
        # Get the last transaction date for the stock
        last_transaction = DailyTransactions.objects.filter(stock=stock).order_by('-trade_date').first()
        from_date = last_transaction.trade_date.strftime('%d-%m-%Y') if last_transaction else '01-01-2024'
        today = datetime.today()
        to_date = today.strftime('%d-%m-%Y') # Get the current date
        data = get_transaction_data_for_stock(stock_code, from_date, to_date)
        if data:
            for row in data['data']:

                print(row)

                trade_date = row['Date']
                day_open = row['Open Price']
                day_high = row['High Price']
                day_low = row['Low Price']
                day_close = row['Close Price']
                volume_traded = row['Total Traded Quantity']
                volume_delivered = row['Deliverable Qty']
                traded_value = row['Turnover ₹']
                ratio_delv_to_traded = row['% Dly Qt to Traded Qty']
                # Check if the record already exists
                # if DailyTransactions.objects.filter(stock=stock, trade_date=trade_date).exists():
                #     print(f"Record for {stock_code} on {trade_date} already exists")
                #     continue
                # DailyTransactions.objects.create(
                #     stock=stock,
                #     trade_date=trade_date,
                #     day_open=day_open,
                #     day_high=day_high,
                #     day_low=day_low,
                #     day_close=day_close,
                #     volume_traded=volume_traded,
                #     volume_delivered=volume_delivered,
                #     traded_value=traded_value,
                #     ratio_delv_to_traded=ratio_delv_to_traded
                # )
                # print(f"Record for {stock_code} on {trade_date} created")
        else:
            print(f"Error in fetching data for {stock_code}")

def fetch_data_from_nse(stock_code):
    stock_code = stock_code.upper()
    print(f"Fetch data from NSE for  {stock_code}")
    base_url = 'https://www.nseindia.com/report-detail/eq_security'
    from_date = '01-07-2024'
    to_date = datetime.today().strftime('%d-%m-%Y')
    headers = get_adjusted_headers()
    headers['Referer'] = base_url
    initial_req = requests.get(base_url, timeout=30, headers=headers)
    stock_obj = None

    if initial_req.status_code == 200:
        new_rec = 0
        updated_rec = 0
        print("Initial request successful")
        # Extract cookies from the initial request
        cookies = initial_req.cookies.get_dict()

        url = f"https://www.nseindia.com/api/historical/securityArchives?from={from_date}&to={to_date}&symbol={stock_code}&dataType=priceVolumeDeliverable&series=ALL"
        '''
        'CH_SYMBOL': 'ZOMATO', 'CH_SERIES': 'EQ', 'CH_MARKET_TYPE': 'N', 'CH_TIMESTAMP': '2024-07-01', 'TIMESTAMP': '2024-06-30T18:30:00.000Z', 'CH_TRADE_HIGH_PRICE': 204.5, 'CH_TRADE_LOW_PRICE': 194.56, 'CH_OPENING_PRICE': 199.71, 'CH_CLOSING_PRICE': 203.97, 'CH_LAST_TRADED_PRICE': 203.7, 'CH_PREVIOUS_CLS_PRICE': 200.56, 'CH_TOT_TRADED_QTY': 53536850, 'CH_TOT_TRADED_VAL': 10769399698.23, 'CH_52WEEK_HIGH_PRICE': 207.2, 'CH_52WEEK_LOW_PRICE': 73, 'CH_TOTAL_TRADES': 405038, 'CH_ISIN': 'INE758T01015', 'createdAt': '2024-07-01T12:00:58.273Z', 'updatedAt': '2024-07-01T12:00:58.273Z', '__v': 0, 'COP_DELIV_QTY': 25817970, 'COP_DELIV_PERC': 48.22, 'VWAP': 201.16, 'mTIMESTAMP': '01-Jul-2024'}

        '''
        response = requests.get(url, timeout=30, headers=get_adjusted_headers(), cookies=cookies)
        if response.status_code == 200:
            try:
                stock_obj, created = Stock.objects.get_or_create(stock_code=stock_code)
                print(f"Upload data for stock {stock_obj}")
            except Exception as e:
                print(f"Exception in stock creation for {e}")
                return None
            resp_data = response.json()['data']
            for row in resp_data:
                type = row['CH_SERIES']
                if type != EQUITY:
                    print(f"Skipping non-equity data for {stock_code}")
                    continue
                trade_date = row['CH_TIMESTAMP']
                previous_close = row['CH_PREVIOUS_CLS_PRICE']
                day_open = row['CH_OPENING_PRICE']
                day_high = row['CH_TRADE_HIGH_PRICE']
                day_low = row['CH_TRADE_LOW_PRICE']
                day_close = row['CH_CLOSING_PRICE']
                vwap = row['VWAP']
                volume_traded = row['CH_TOT_TRADED_QTY']
                volume_delivered = row['COP_DELIV_QTY']
                traded_value = row['CH_TOT_TRADED_VAL']
                ratio_delv_to_traded = row['COP_DELIV_PERC']
                # print(trade_date, previous_close, day_open, day_high, day_low, day_close, vwap, volume_traded,
                #       volume_delivered, traded_value, ratio_delv_to_traded)
                try:
                    print(f"{stock_code}\t{trade_date}")

                    daily_transaction, created = DailyTransactions.objects.get_or_create(
                        stock=stock_obj,
                        trade_date=trade_date,
                        defaults={
                            'previous_close': previous_close,
                            'day_open': day_open,
                            'day_high': day_high,
                            'day_low': day_low,
                            'day_close': day_close,
                            'vwap': vwap,
                            'volume_traded': volume_traded,
                            'volume_delivered': volume_delivered,
                            'traded_value': traded_value,
                            'ratio_delv_to_traded': ratio_delv_to_traded
                        }
                    )
                    if created:
                        new_rec += 1
                        # print(f"New record created for {stock_code} for {trade_date}")
                    else:
                        daily_transaction.previous_close = previous_close
                        daily_transaction.day_open = day_open
                        daily_transaction.day_high = day_high
                        daily_transaction.day_low = day_low
                        daily_transaction.day_close = day_close
                        daily_transaction.vwap = vwap
                        daily_transaction.volume_traded = volume_traded
                        daily_transaction.volume_delivered = volume_delivered
                        daily_transaction.traded_value = traded_value
                        daily_transaction.ratio_delv_to_traded = ratio_delv_to_traded
                        daily_transaction.save()
                        updated_rec += 1
                        # print(f"Record updated for {stock_code} for {trade_date}")
                except Exception as e:
                    print(f"Error in saving data for {stock_code} for {trade_date}: {e}")
            print(f"Total records uploaded {len(resp_data)} New records {new_rec} Updated records {updated_rec}")
        else:
            print(
                f"Error in fetching data for {stock_code} from {from_date} to {to_date} with status code {response.status_code}")
            print(response.text)
    else:
        print(f"Error in initial request with status code {initial_req.status_code}")
        print(initial_req.text)
        return None
