
from .constants import chaldean_mapping

MASTER_NUMBERS = [11, 22, 33, 44, 55, 66, 77, 88, 99]


def cal_unity_root(input_data, remainder):
    if str(input_data).find(".") != -1:
        input_data = int(str(input_data).replace(".", ""))
    if input_data == 0:
        if remainder >= 10:
            return cal_unity_root(remainder, 0)
        return remainder
    else:
        if input_data in MASTER_NUMBERS:
            return input_data
        r = input_data % 10
        return cal_unity_root(int(input_data / 10), remainder + r)


def get_unity_number_for_scrip(name):
    name = name.upper()
    total_sum = 0
    for letter in name:
        #find key for letter from chaldean_mapping
        for key, value in chaldean_mapping.items():
            if letter in value:
                total_sum += key
    print("Total sum", name, total_sum)
    unity_root = cal_unity_root(total_sum, 0)
    print("Unity root", unity_root)
    return unity_root