from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, UpdateByQuery

from stocks.models import Stock, DailyTransactions

index_name = 'daily_transactions'

def upload_data_to_elastic(data, index_name, doc_type, id_field):
    es = Elasticsearch()
    for item in data:
        es.index(index=index_name, doc_type=doc_type, id=item[id_field], body=item)
    return True


def update_daily_transactions():
    es = Elasticsearch()
    s = Search(using=es, index=index_name)
    s = s.filter('range', trade_date={'gte': 'now-1d/d', 'lt': 'now/d'})
    s = s.source(['stock', 'trade_date'])
    response = s.execute()
    for hit in response:
        stock = Stock.objects.get(stock_code=hit.stock)
        daily_transaction = DailyTransactions.objects.get(stock=stock, trade_date=hit.trade_date)
        daily_transaction.save()
    return True