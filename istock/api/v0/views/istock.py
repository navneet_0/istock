from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from api.v0.serializers.istock import StockDetailsSerializer, StockSerializer
from rest_framework import viewsets
from rest_framework.response import Response

from stocks.models import Stock, StockDetails


class StockViewSet(viewsets.ModelViewSet):
    serializer_class = StockSerializer
    queryset = Stock.objects.all()

    def create(self, request):
        print("Create request", request.data)
        request.data['stock_code'] = request.data['stock_code'].upper()
        request.data['stock_name'] = request.data['stock_name'] if request.data.get('stock_name') else request.data['stock_code']
        serializer = StockSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            print(serializer.errors)
            return Response(serializer.errors, status=400)
        # post_data = request.data
        # stock_code = post_data.get('stock_code').upper()
        # stock_name = post_data.get('stock_name', None)
        # if stock_name is None:
        #     stock_name = stock_code
        # stock = Stock.objects.create(stock_code=stock_code, stock_name=stock_name)
        # #get serializer object and send in response
        # serializer = StockSerializer(stock)
        # if serializer.is_valid():
        #     return Response(serializer.data)
        # else:
        #     return Response(serializer.errors)


class StockDetailsViewSet(viewsets.ViewSet):
    serializer_class = StockDetailsSerializer
    queryset = StockDetails.objects.all()
    """
    A simple ViewSet for listing or retrieving users.
    """
    def list(self, request):
        print("List request")
        queryset = StockDetails.objects.all()
        serializer = StockDetailsSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = StockDetails.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = StockDetailsSerializer(user)
        return Response(serializer.data)