from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response
from django_filters import rest_framework as filters

from django_filters import DateFromToRangeFilter


from stocks.models import DailyTransactions, Stock, WeeklyData

from api.v0.serializers.transactions import DailyTransactionsSerializer, WeeklyDataSerializer


class DailyTransactionsFilter(filters.FilterSet):
    trade_date = DateFromToRangeFilter()

    class Meta:
        model = DailyTransactions
        fields = ['stock', 'trade_date']


class DailyTransactionsViewSet(viewsets.ModelViewSet):
    serializer_class = DailyTransactionsSerializer
    # __basic_fields = ('stock', 'trade_date',)
    filter_backends = (filters.DjangoFilterBackend,)
    # filterset_fields = __basic_fields
    filterset_class = DailyTransactionsFilter

    def get_queryset(self):
        stock_code = self.request.query_params.get('stock_code', None)
        if stock_code is not None:
            try:
                stock = Stock.objects.get(stock_code=stock_code.upper())
            except Stock.DoesNotExist:
                return DailyTransactions.objects.none()
            queryset = DailyTransactions.objects.filter(stock=stock).order_by('-trade_date')
        else:
            queryset = DailyTransactions.objects.none()
        return queryset

    def list(self, request, *args, **kwargs):
        resp_data = super(DailyTransactionsViewSet, self).list(request, *args, **kwargs)
        return resp_data


class WeeklyDataViewSet(viewsets.ModelViewSet):
    serializer_class = WeeklyDataSerializer

    def get_queryset(self):
        stock_code = self.request.query_params.get('stock_code', None)
        if stock_code is not None:
            try:
                stock = Stock.objects.get(stock_code=stock_code)
            except Stock.DoesNotExist:
                return WeeklyData.objects.none()
            queryset = WeeklyData.objects.filter(stock=stock).order_by('-week_number')
        else:
            queryset = WeeklyData.objects.none()
        return queryset

    def list(self, request, *args, **kwargs):
        resp_data = super(WeeklyDataViewSet, self).list(request, *args, **kwargs)
        return resp_data
