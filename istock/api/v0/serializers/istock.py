from rest_framework import serializers

from stocks.models import StockDetails, Stock


class StockSerializer(serializers.ModelSerializer):
    # name_unity_root = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Stock
        fields = '__all__'
        # fields = ['stock_code', 'stock_name', 'name_unity_root']

    def get_name_unity_root(self, obj):
        # Here we access the property method 'name_unity_root' of the Stock model instance
        return obj.name_unity_root


class StockDetailsSerializer(serializers.Serializer):
    class Meta:
        model = StockDetails
        fields = '__all__'
