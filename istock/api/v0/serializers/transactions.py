import datetime

from rest_framework import serializers

from stocks.models import DailyTransactions, WeeklyData


class DailyTransactionsSerializer(serializers.ModelSerializer):
    volume_traded_ratio = serializers.SerializerMethodField()
    volume_delivered_ratio = serializers.SerializerMethodField()
    gap_opening = serializers.SerializerMethodField()
    opening_price_unity_root = serializers.SerializerMethodField()
    close_price_unity_root = serializers.SerializerMethodField()
    high_price_unity_root = serializers.SerializerMethodField()
    low_price_unity_root = serializers.SerializerMethodField()
    current_week = serializers.SerializerMethodField()


    class Meta:
        model = DailyTransactions
        fields = '__all__'

    def get_volume_traded_ratio(self, obj):
        return obj.volume_traded_ratio

    def get_volume_delivered_ratio(self, obj):
        return obj.volume_delivered_ratio

    def get_gap_opening(self, obj):
        # Here we access the property method 'gap_opening' of the Stock model instance
        return obj.gap_opening

    def get_opening_price_unity_root(self, obj):
        return obj.opening_price_unity_root

    def get_close_price_unity_root(self, obj):
        return obj.closing_price_unity_root

    def get_high_price_unity_root(self, obj):
        return obj.high_price_unity_root

    def get_low_price_unity_root(self, obj):
        return obj.low_price_unity_root

    def get_current_week(self, obj):
        return obj.current_week


class WeeklyDataSerializer(serializers.ModelSerializer):
    week_price_range_ratios = serializers.SerializerMethodField()
    week_delv_to_traded = serializers.SerializerMethodField()

    class Meta:
        model = WeeklyData
        fields = '__all__'


    def get_week_price_range_ratios(self, obj):
        return obj.week_price_range_ratios

    def get_week_delv_to_traded(self, obj):
        return obj.week_delv_to_traded



