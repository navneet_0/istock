// renderer.js
import { getListOfStocks } from './src/scripts/getlistofstocks';

document.addEventListener('DOMContentLoaded', () => {
    console.log('DOMContentLoaded');
    getListOfStocks()
        .then(list => console.log(list))
        .catch(error => console.error(error));
});