// api.js
import axios from 'axios';

// Create an Axios instance
const api = axios.create({
    baseURL: 'http://127.0.0.1:9100/api/v0/', // Replace with your API base URL
    timeout: 10000, // Optional: Set a timeout for requests
    headers: {
        'Content-Type': 'application/json',
        // Add any other default headers here
    },
});

// Optionally, set up interceptors for request and response
api.interceptors.request.use(
    (config) => {
        // You can add auth tokens or modify the request config here
        // Example: config.headers['Authorization'] = 'Bearer YOUR_TOKEN';
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

api.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        // Handle errors globally here
        return Promise.reject(error);
    }
);

export default api;