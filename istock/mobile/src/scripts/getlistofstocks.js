import api from "../../components/utils/httpReq";

export async function getListOfStocks() {
    try {
        console.log('getListOfStocks');
        const response = await api.get('/stocks/');
        return response.data;
    } catch (error) {
        console.error('Error fetching stocks:', error);
        throw error;
    }
}